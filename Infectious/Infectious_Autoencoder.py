import numpy as np
import pandas as pd
from keras.layers import Dense, Input
from keras.models import Model

#### Read in the csv
df = pd.read_csv('Infection.csv')
df_new = df.iloc[:, 1:]

minimum_time = min(df_new.iloc[:,-1])
df_new['New Time (s)'] = df_new['Time'].map(lambda x: x - minimum_time)
df_sorted = df_new.sort_values('New Time (s)')
nodes = max(max(df_sorted['Node 1'].unique()), max(df_sorted['Node 2'].unique()))

#### Function to make the arrays
def get_adj_time_range(timerange, dataframe):
    x = np.zeros((nodes, nodes))
    dataframe_slice = dataframe.loc[
        (dataframe['New Time (s)'] >= timerange[0]) & (dataframe['New Time (s)'] <= timerange[1])]
    for i, j in zip(dataframe_slice['Node 1'], dataframe_slice['Node 2']):
        x[i - 1][j - 1] += 1

    return x

#### Creating the training data
timestep = 10
train = []
for i in range(0, max(df_sorted['New Time (s)']) , timestep):
    matrix = get_adj_time_range((i, i + timestep), df_sorted)
    train.append(np.reshape(matrix, (nodes ** 2)))
train_np = np.asarray(train)
print('Train Shape: {}'.format(train_np.shape))

#### Creating the network
input_vec = Input(shape=(nodes ** 2, ))
encode = Dense(128, activation='relu')(input_vec)
encode = Dense(64, activation='relu')(encode)

decode = Dense(128, activation='relu')(encode)
decode = Dense( nodes ** 2)(decode)

model = Model(input_vec, decode)
model.compile(optimizer='adadelta', loss='mse')
model.fit(train_np, train_np, epochs=15, batch_size=64, shuffle=True)


