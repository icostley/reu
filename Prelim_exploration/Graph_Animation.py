import pylab
import random
import matplotlib.pyplot as plt
import networkx as nx
# import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation
import numpy as np
import sys
import seaborn
import time

fig, ax = plt.subplots()
fig.set_tight_layout(False)
plt.cla()

# Query the figure's on-screen size and DPI. Note that when saving the figure to
# a file, we need to provide a DPI for that separately.
print('fig size: {0} DPI, size in inches {1}'.format(
    fig.get_dpi(), fig.get_size_inches()))

## Plot a scatter that persists (isn't redrawn) and the initial line.
# x = np.arange(0, 6, 0.1)
# ax.scatter(x, x + np.random.normal(0, 3.0, len(x)))
# line, = ax.plot(x, x - 2, 'r-', linewidth=2)

# G = nx.Graph()
# G.add_node(1)
# G.add_nodes_from(range(2,8))
# nx.draw(G, node_color=(172.0/255.0, 43.0/255.0, 55.0/255.0))

# pos = {1: (1,50), 2: (30,60)}
def update(i):
    label = 'timestep {0}'.format(i)
    print(label)
    plt.clf()
    G = nx.Graph()
    G.add_nodes_from(range(1,3))
    nx.draw_networkx(G, node_color=(172.0/255.0, 43.0/255.0, 55.0/255.0))
    time.sleep(2)

    G.add_edge(1,2)
    # nx.draw(G,pos, node_color=(172.0/255.0, 43.0/255.0, 55.0/255.0))
    time.sleep(0.5)
    # Update the line and the axes (with a new xlabel). Return a tuple of
    # "artists" that have to be redrawn for this frame.
    # line.set_ydata(x - 2 + i)
    # ax.set_xlabel(label)
    # return  ax

if __name__ == '__main__':
    # FuncAnimation will call the 'update' function for each frame; here
    # animating over 10 frames, with an interval of 200ms between frames.
    anim = FuncAnimation(fig, update, frames=np.arange(0, 10), interval=200)
    if len(sys.argv) > 1 and sys.argv[1] == 'save':
        anim.save('line.gif', dpi=80, writer='imagemagick')
    else:
        # plt.show() will just loop the animation forever.
        plt.show()