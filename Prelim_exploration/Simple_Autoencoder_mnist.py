from keras.layers import Input, Dense
from keras.models import Model
from keras.callbacks import TensorBoard
import matplotlib.pyplot as plt
import numpy as np


# (x_train, _), (x_test, _) = mnist.load_data()
#
# x_train = x_train.astype('float32') / 255
# x_test = x_test.astype('float32') / 255
#
# print(len(x_train))
# x_train = np.resize(x_train, (len(x_train), x_train.shape[1] * x_train.shape[2]))
# x_test = np.resize(x_test, (len(x_test), x_test.shape[1] * x_test.shape[2]))
#

x_train = np.random.rand(60000, 784)



# Autoencoder
input_img = Input(shape=(784,))
encoded = Dense(128, activation='relu')(input_img)
encoded = Dense(64, activation='relu')(encoded)
encoded = Dense(32, activation='relu')(encoded)
encoded = Dense(16, activation='relu')(encoded)

decoded = Dense(32, activation='relu')(encoded)
decoded = Dense(64, activation='relu')(decoded)
decoded = Dense(128, activation='relu')(decoded)
decoded = Dense(784, activation='sigmoid')(decoded)

autoencoder = Model(input_img, decoded)
autoencoder.compile(optimizer='adadelta', loss='binary_crossentropy')
autoencoder.fit(x_train, x_train, epochs=10, batch_size=128, shuffle=True, validation_data=(x_train, x_train), callbacks=[TensorBoard(log_dir='logs')])



autoencoder.save('Test_model.h5')
# Visualizing how well the autoencoder works
# decoded_imgs  = autoencoder.predict(x_train)
#
# n = 10
# plt.figure(figsize=(20,4))
# for i in range(n):
#     # display original
#     ax = plt.subplot(2, n, i + 1 )
#     plt.imshow(x_train[i].reshape(28,28))
#     plt.gray()
#     ax.get_xaxis().set_visible(False)
#     ax.get_yaxis().set_visible(False)
#
#     # display reconstruction
#     ax = plt.subplot(2, n, i + n +1)
#     plt.imshow(decoded_imgs[i].reshape(28,28))
#     plt.gray()
#     ax.get_xaxis().set_visible(False)
#     ax.get_yaxis().set_visible(False)
# plt.savefig('Test_img.png')


