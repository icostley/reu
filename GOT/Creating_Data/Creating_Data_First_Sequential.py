import pandas as pd
from copy import deepcopy
import numpy as np

# Loading in the data
df = pd.read_csv('GOT27.csv')
nodes = max(max(df['Node_1'].unique() + 1), max(df['Node_2'].unique() + 1))

matrix_width = 5
time_step = 200

# Necessary Functions

def create_vector(timerange, dataframe, nodes):
    '''
    create_vector

    requires: tuple timerange, data datframe, int nodes
    modifies: nothing
    effects: Creates a vector of size nodes squared - nodes out of dataframe in timerange
      Essentially taking data and making adjacency matrix in one dimension vector
    '''

    global x

    y = deepcopy(dataframe)

    data_create_vector = y.loc[(y['Start'] >= timerange[0]) & (y['Start'] <= timerange[1])]

    x = np.zeros((int((nodes ** 2 - nodes) / 2),))

    data_create_vector.apply(update_matrix, axis=1)

    return x


def update_matrix(source_target_pair):
    set_ = (source_target_pair[0], source_target_pair[1])
    reset = (min(set_) - 1, max(set_) - 1)
    index = nodes * (reset[0]) - reset[0] * (1 + reset[0]) / 2 + (reset[1] - reset[0] - 1)

    x[int(index)] += 1


def training_data_seq(dataframe_train, time_step, matrix_width):
    '''
    training_data
    requires: dataframe_train w Source, Start, Target columns, int time_step, int matrix_width,
    int sample_size
    modifes: nothing
    effects: puts vectors in training set and validation set
     NOTE: same as training_data_generator but not a generator
    '''
    X_train = []
    Y_train = []
    max_time = max(dataframe_train['Start'])

    # total number of nodes
    nodes = max(max(dataframe_train['Node_1'].unique() + 1), max(dataframe_train['Node_2'].unique() + 1))

    # Number of training samples to be made
    max_iter = int((max_time / time_step) / (1 + matrix_width))
    for i in range(max_iter):
        # Initialize the time to start at
        time = i * (time_step * (matrix_width + 1))
        x_batch = []
        # this loop iterates through and creates a pack for training
        for j in range(matrix_width + 1):
            # the if statement sets aside one y vector at the end of every pack
            if j == (matrix_width):
                y_batch = create_vector((time + j * time_step, time + (j + 1) * time_step),
                                        dataframe_train, nodes)
            else:
                x_batch.append(create_vector((time + j * time_step, time + (j + 1) * time_step),
                                             dataframe_train, nodes))

        # add batches to training data
        X_train.append(x_batch)
        Y_train.append(y_batch)

    return (np.asarray(X_train), np.asarray(Y_train))


# Creating the data
X_train_np , y_train_np = training_data_seq(df, time_step=time_step, matrix_width=matrix_width)

np.save('X_seq_train_mat_{}_time_{}'.format(matrix_width, time_step), X_train_np)
np.save('y_seq_train_mat_{}_time_{}'.format(matrix_width, time_step), y_train_np)
