#TODO: Implement argparser for edge or time_step

import pandas as pd
from keras.layers import Dense, Input, BatchNormalization, Flatten
from keras.models import Model
import os
import numpy as np
import tensorflow as tf
import pickle
from copy import deepcopy
import argparse
from Reality_functions import *
from models_ex import models
import os
from metrics_ex import *

# Load the data
path_to_reality = os.path.join('/home/icostley/reu/Reality_mining/Creating_Data', 'reality.csv')
df_init = pd.read_csv(path_to_reality)
df = df_init.drop('End', axis=1)
nodes = max(max(df['Source'].unique()), max(df['Target'].unique()))

# Defining initial variables
# They can be changed with the arg parser
matrix_width = 5
vector_length = int((nodes ** 2 - nodes) / 2)
validation_size = 10000
time_step = 1000
metric_list = [total_acc, y_true_non_zero, non_zero]
layers = None
model_type = 'real_auto'
epochs = 50
batch_size = 128
optimizer = 'adadelta'
loss_function = 'mse'



if __name__ == '__main__':
    # Define the argpaser
    parser = argparse.ArgumentParser()
    parser.add_argument("--matrix_width", type=int, help='Pack size for validation')
    parser.add_argument("--time_step", type=int, help='Time interval for snapshot')
    parser.add_argument("--validation_size",type=int,  help='Validation set size')
    parser.add_argument("--model_type", help='What type of model to train')
    parser.add_argument("--optimizer", help='What optimizer to use for back prop')
    parser.add_argument("--metric_list", help='What metrics to use for the function')
    parser.add_argument("--loss_function", help='Loss function for training the model')
    parser.add_argument("--path_to_x", help='What is the path to x training data')
    parser.add_argument("--path_to_y", help="What is the path to y training data")
    parser.add_argument("--batch_size", type=int, help='Batch size for training the network')
    parser.add_argument("--epochs", type=int, help='Number of epochs to train the model')
    parser.add_argument("--layers", help='Layer tuple for the network')
    args = parser.parse_args()

    #Notice that the order matters here
    if args.matrix_width:
        matrix_width = args.matrix_width
    if args.time_step:
        time_step = args.time_step
    if args.validation_size:
        validation_size = args.validation_size
    if args.model_type:
        model_type = str(args.model_type)
    if args.optimizer:
        optimizer = str(args.optimizer)
    if args.metric_list:
        metric_list = args.metric_list
    if args.loss_function:
        loss_function = args.loss_function
    if args.path_to_x:
        dir_path = '/home/icostley/reu/Reality_mining/Creating_Data'
        path_to_X = os.path.join(dir_path, args.path_to_x)
    else:
        path_to_X = os.path.join('/home/icostley/reu/Reality_mining/Creating_Data', ('X_train_mat_{}_time_{}.npy'.format(matrix_width, time_step)))
    if args.path_to_y:
        dir_path = '/home/icostley/reu/Reality_mining/Creating_Data'
        path_to_y = os.path.join(dir_path, args.path_to_y)
    else:
        path_to_y = os.path.join('/home/icostley/reu/Reality_mining/Creating_Data', ('y_train_mat_{}_time_{}.npy'.format(matrix_width, time_step)))
    if args.batch_size:
        batch_size = args.batch_size
    if args.epochs:
        epochs = args.epochs
    if args.layers:
        layers = eval(args.layers)

    # Loading in the training data
    X_train = np.load(path_to_X)

    #For feature extraction, we do not need the y data



    # Create the validation data
    # Again, we do not need the y-validation
    validation_gen = training_data_generator(df, time_step=time_step, matrix_width=matrix_width)
    X_validate = []
    for j,i in enumerate(validation_gen):
        if j == (validation_size - 1):
            break
        X_validate.append(i[0])

    X_validate = np.asarray(X_validate)

    # Define the model
    model = eval('models({}, {}, {} ).{}()'.format(layers, vector_length, matrix_width, model_type))

    model.compile(optimizer=optimizer, metrics=metric_list, loss=loss_function)
    history = model.fit(x=X_train, y=X_train, batch_size=batch_size, epochs=epochs, validation_data = (X_validate, X_validate))

    # Saving all objects
    model.save('{}_{}_{}_{}_mat_{}_time_{}.h5'.format(model_type, optimizer, loss_function, layers, matrix_width, time_step))

    # Saving the history objects
    with open('history_{}_{}_{}_{}_mat_{}_time_{}'.format(model_type, optimizer, loss_function, layers, matrix_width, time_step), 'wb') as pifile:
        pickle.dump(history.history, pifile, protocol=pickle.HIGHEST_PROTOCOL)