'''
Reality_functions.py

REU 2018: Predicting Temporal Netowork Data

Marisa O'Gara, Ian Costley

Contains all of our functions for analyzing undirected time series network data.
Specifically designed for reality mining data
'''
import numpy as np
from copy import deepcopy
from tqdm import tqdm
nodes = 96


def create_vector(timerange, dataframe, nodes):
    '''
    create_vector

    requires: tuple timerange, data datframe, int nodes
    modifies: nothing
    effects: Creates a vector of size (nodes squared - nodes) / 2 out of dataframe in timerange
      Essentially taking data and making adjacency matrix in one dimension vector
    '''

    #x is the vector to be created & global so it can be changed by update_matrix 
    global x
    x = np.zeros((int((nodes ** 2 - nodes) / 2),))

    y = deepcopy(dataframe)

    #data_create_vector dataframe restricted to the data in the correct timerange
    data_create_vector = y.loc[(y['Start'] >= timerange[0]) & (y['Start'] <= timerange[1])]

    #iterates through data_create_vector with function update_matrix to finish vector
    data_create_vector.apply(update_matrix, axis=1)

    return x


def create_vector_index(index_tuple, dataframe, nodes):
    '''
    create_vector_index

    requires: tuple index_tuple, data dataframe, int nodes
    modifies: nothing
    effects: creates a vector of sizes (nodes squared - nodes) / 2 out of data from dataframe in
        the range of the given index tuple
    '''
  
    global x
    x = np.zeros((int((nodes ** 2 - nodes) / 2),))
  
    y = deepcopy(dataframe)
  
    #create vector using data from the first index to the second/last index
    data_create_vector = y.iloc[int(index_tuple[0]):int(index_tuple[1])]
    data_create_vector.apply(update_matrix, axis=1)
     
    return x
  


def update_matrix(source_target_pair):
    '''
    update_matrix

    requires: tuple? source_target_pair
    modifies: x
    effects: adds a 1 to the right spot in x
    '''
    #source has index 0, target has index 1

    set_ = (source_target_pair[0], source_target_pair[1])

    #reset gets the indices in order and accounts for an off by one error
    reset = (min(set_) - 1, max(set_) - 1)

    #index is actually where we want to add the one in the 1D vector
    index = nodes * (reset[0]) - reset[0] * (1 + reset[0]) / 2 + (reset[1] - reset[0] - 1)


    x[int(index)] += 1


def get_adj_time_range(timerange, dataframe, nodes):

    '''
    get_adj_time_range
    (originally from infectious data)

    requires: tuple timerange, dataframe, int nodes
    modifies: nothing
    effects: creates an adjacency matrix given a timeframe for a dataframe.
    Important for finding the eigenvector centrality
    '''
    df_copy = deepcopy(dataframe)

    #take slice of dataframe based on time
    dataframe_slice = df_copy.loc[(dataframe['Start'] >= timerange[0]) & (df_copy['Start'] <= timerange[1])]

    x = np.zeros((nodes, nodes))

    #fill adj. matrix
    for i, j in zip(dataframe_slice['Source'], dataframe_slice['Target']):
        x[int(i - 1)][int(j - 1)] += 1
        x[int(j -1)][int(i - 1)] += 1
    
    return x


def eigen_centrality(adjacency_matrix):
  '''
  eigen_centrality

  requires: np matrix adjacency matrix
  modifies: nothing
  effects: returns the eigen vector centrality for given adj.matrix
  '''
  #eigen_all is the eigenvalues and eigenvectors in a single matrix
  eigen_all = np.linalg.eig(adjacency_matrix)
  
  #index of the largest eigenvalue
  index = np.argmax(eigen_all[0])

  #finds the corresponding eigenvector to the largest eigenvalue
  eigen_vector = np.real(eigen_all[1][index])

  #returns normalized eigenvector
  normalized = eigen_vector / np.linalg.norm(eigen_vector)
  return normalized


def training_data_generator(dataframe_train, time_step, matrix_width):
    '''
    training_data_generator

    :param dataframe_train: Data frame that has a "Start", "Source", and "Target" column
    :param time_step: What time step should each vector represent
    :param matrix_width: How many vector per x sample
    :return: tuple of x and y

    effects: Generates training data 

    '''

    max_time = max(dataframe_train['Start'])

    # df_new is adjusted for time bounds when a pack is going to be created
    df_new = dataframe_train.loc[dataframe_train['Start'] <= (max_time - time_step * (matrix_width + 1))]

    #overall total number of nodes
    nodes = max(max(dataframe_train['Source'].unique()), max(dataframe_train['Target'].unique()))

    while (1):
        #i is a random index that can be used to start a training sample from
        i = np.rint(np.random.uniform(low=0, high=(len(df_new) - 1)))

        #time is where the start time of the ith data point
        time = df_new['Start'].iloc[int(i)]

        x_batch = []
        y_batch = None

        #create the packs for the training data
        for j in range(matrix_width + 1):
            #timerange gives the sanpshot in which we want to create the vector

           #if statment so the last vector is always a y vector
            if j == (matrix_width):
                y_batch = create_vector((time + j * time_step, time + (j + 1) * time_step), dataframe_train, nodes)
            else:
                x_batch.append(create_vector((time + j * time_step, time + (j + 1) * time_step), dataframe_train, nodes))

        yield (np.asarray(x_batch), np.asarray(y_batch))

def training_data_edge(dataframe_train, edge_number, matrix_width, sample_size):
    '''
    training_data_edge
    requires:  dataframe_train w Source, Start, Target columns, int edge_number, int matrix_width,
    int sample_size
    modifes: nothing
    effects: puts vectors in training set and validation set based on number of edges per vector
    '''
    X_train = []
    y_train = []

    max_length = len(dataframe_train)

    #max index you can start making a pack without running out of space
    index_max = max_length - (edge_number * (matrix_width + 1))

    nodes = max(max(dataframe_train['Source'].unique()), max(dataframe_train['Target'].unique()))

    #list of random indices that can be used to make new packs, of length sample_size
    index_list = np.rint(np.random.uniform(low=0, high=(index_max), size=sample_size))

    for i in index_list:
        
        x_batch = []
        #this loop iterates through and creates a pack for training
        for j in range(matrix_width + 1):
            #the if statement sets aside one y vector at the end of every packl
            if j == (matrix_width):
                y_batch = create_vector_index(((i + (j * edge_number)), i + ((j + 1) * edge_number)),
                    dataframe_train, nodes)

            else:
                x_batch.append(create_vector_index( ((i + (j * edge_number)), i + ((j + 1) * edge_number)),
                    dataframe_train, nodes) )

            X_train.append(x_batch)
            y_train.append(y_batch)

    return np.asarray(X_train), np.asarray(y_train)
  

def training_data(dataframe_train, time_step, matrix_width, sample_size):
    '''
    training_data
    requires: dataframe_train w Source, Start, Target columns, int time_step, int matrix_width, 
    int sample_size
    modifes: nothing
    effects: puts vectors in training set and validation set
     NOTE: same as training_data_generator but not a generator
    '''
    X_train = []
    Y_train = []
    max_time = max(dataframe_train['Start'])

    #df_new is adjusted for time bounds
    df_new = dataframe_train.loc[dataframe_train['Start'] <= (max_time - time_step * (matrix_width + 1))]
  
    #total number of nodes
    nodes = max(max(dataframe_train['Source'].unique()), max(dataframe_train['Target'].unique()))
 
    #list of random indices that can be used to make new packs, of length sample_size
    index_list = np.rint(np.random.uniform(low=0, high=(len(df_new) - 1), size=sample_size))
  

    for i in tqdm(index_list):

        #the start time for the first edge (which has index i)
        time = df_new['Start'].iloc[int(i)]
        x_batch = []
        #this loop iterates through and creates a pack for training
        for j in range(matrix_width + 1):
            #the if statement sets aside one y vector at the end of every packl
            if j == (matrix_width):
                y_batch = create_vector((time + j * time_step, time + (j + 1) * time_step),
                 dataframe_train, nodes)
            else:
                x_batch.append(create_vector((time + j * time_step, time + (j + 1) * time_step),
                 dataframe_train, nodes))
    
        #add batches to training data
        X_train.append(x_batch)
        Y_train.append(y_batch)
    
    return (np.asarray(X_train), np.asarray(Y_train))


def training_eigen_1(dataframe_train, time_step, matrix_width, sample_size):
    '''
    training_eigen_1

    requires: dataframe_train, int time_step, int matrix_width, int sample_size
    modifies: nothing
    effect: creates training data with eigenvector centrality incoperated by adding all of the nodes eigenvector
        centrality to the end of the vector
    '''
    X_train = []
    Y_train = []
    max_time = max(dataframe_train['Start'])
  
    #df_new is dataframe_train adjusted for time bounds
    df_new = dataframe_train.loc[dataframe_train['Start'] <= (max_time - time_step * (matrix_width + 1))]
  
    #number of nodes in graph
    nodes = max(max(dataframe_train['Source'].unique()), max(dataframe_train['Target'].unique()))
  
    #random indices from df_new as a list of length sample_size
    index_list = np.rint(np.random.uniform(low=0, high=(len(df_new) - 1), size = sample_size))
  
    #create the packs for training data
    for i in index_list:
    
        time = df_new['Start'].iloc[int(i)]
        x_batch = []
        y_batch = []
    
    for j in range(matrix_width + 1):
        #timerange to look at for a single vector
        timerange = (time + j * time_step, time + (j + 1) * time_step)

        #creates eigen vector and 'normal' first order vector
        eigen_vector = eigen_centrality(get_adj_time_range(timerange, dataframe=dataframe_train, nodes=nodes))
        first_order_vector = list(create_vector(timerange, dataframe=dataframe_train, nodes=nodes))
      
        #adds eigen vector to end of first_order_vector
        for vec in eigen_vector:
            first_order_vector.append(vec)
      
        #adds y vector to pack
        if j == (matrix_width):
            y_batch.append(first_order_vector)
        #adds x vectors to pack
        else:
            x_batch.append(first_order_vector)
        
    
        X_train.append(x_batch)
        Y_train.append(y_batch)
    
    return np.asarray(X_train), np.asarray(Y_train)


def training_eigen_2(dataframe_train, time_step, matrix_width, sample_size):
    '''
    training_eigen_2

    requires: dataframe_train, int time_step, int matrix_width, int sample_size
    modifies: nothing
    effect: creates training data with eigenvector centrality incoperated by multiplying every index by the
        two corresponding eigenvector centralities
    '''
    X_train = []
    Y_train = []
    max_time = max(dataframe_train['Start'])

    #df_new is adjusted for time bounds
    df_new = dataframe_train.loc[dataframe_train['Start'] <= (max_time - time_step * (matrix_width + 1))]
  
    #nodes is the max number of nodes 
    nodes = max(max(dataframe_train['Source'].unique()), max(dataframe_train['Target'].unique()))
    
    #list of random indices that can be used to make new packs, of length sample_size
    index_list = np.rint(np.random.uniform(low=0, high=(len(df_new) - 1), size=sample_size))
  

    #makes the sample
    for i in tqdm(index_list):
    
        time = df_new['Start'].iloc[int(i)]
        x_batch = []
        #iterates to create a pack
        for j in range(matrix_width + 1):
      
            #Create the adjacency matrix
            adj_matrix = get_adj_time_range(dataframe=dataframe_train,timerange=(time + j * time_step, time + (j + 1) * time_step), nodes=nodes )
            #gets eigenvector centrality
            eigen_vector_centrality = eigen_centrality(adj_matrix)
            engineered_eigen_vector = []
      
            #Create engineered eigenvectorm of size m * n 
            for m in range(nodes):
                for n in range(m+1, nodes):
                    engineered_eigen_vector.append(eigen_vector_centrality[m] * eigen_vector_centrality[n])
      
            eigen_vector_np = np.asarray(engineered_eigen_vector)
      
            timerange = time + j * time_step, time + (j + 1) * time_step
            #creates the new vectors multiplied by the eigenvector centrality
            if j == (matrix_width):
                y_batch = create_vector(timerange, dataframe_train, nodes) * eigen_vector_np
            else:
                x_batch.append(create_vector(timerange, dataframe_train, nodes) * eigen_vector_np)
        
    
        X_train.append(x_batch)
        Y_train.append(y_batch)
    
    return np.asarray(X_train), np.asarray(Y_train)