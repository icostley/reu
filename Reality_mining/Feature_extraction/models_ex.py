from keras.layers import Dense, Input, LSTM, BatchNormalization, Flatten, Reshape
from keras.models import Model

class models:
    def __init__(self, layers, vector_length, matrix_width):
        self.layers = layers
        self.vector_length = vector_length
        self.matrix_width = matrix_width

    def real_auto(self):
        if self.layers == None:
            self.layers = (2280, 1140, 2280)

        if len(self.layers) != 3:
            raise ValueError('"Layers must be tuple of length 3 for "basic_auto"')

        layer_1, layer_2, layer_3 = self.layers
        input_vector = Input(shape=(self.matrix_width, self.vector_length))
        flatten = Flatten()(input_vector)
        encoded = Dense(layer_1, activation='relu', name='encoded_1')(flatten)
        norm = BatchNormalization()(encoded)
        encoded = Dense(layer_2, activation='relu', name='encoded_2')(norm)
        norm = BatchNormalization()(encoded)

        decoded = Dense(layer_3, activation='relu', name='decoded_1')(norm)
        norm = BatchNormalization()(decoded)
        decoded = Dense((self.vector_length * self.matrix_width), name='decoded_2')(norm)
        reshape = Reshape((5, self.vector_length))(decoded)

        basic_autoencoder = Model(input_vector, reshape)

        return basic_autoencoder

    def baseline_lstm(self):
        if len(self.layers) != 2:
            raise ValueError('"Layers" must be tuple of length 2 for "baseline_lstm"')

        if self.layers == None:
            self.layers = (self.vector_length // 8, self.vector_length // 16)

        layer_1, layer_2 = self.layers
        input_mat = Input(shape=(self.matrix_width, self.vector_length))
        encoded = LSTM(layer_1)(input_mat)
        encoded = LSTM(layer_2)(encoded)

        decoded = LSTM(layer_1)(encoded)
        decoded = Dense(self.vector_length)(decoded)

        baseline_long = Model(input_mat, decoded)

        return baseline_long