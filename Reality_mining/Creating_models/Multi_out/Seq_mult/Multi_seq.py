'''
This file should not need to be made
But we are having troubles with memory managemnt
So all functions and models will be contained within this file
'''


import pandas as pd
from keras.layers import Dense, Input, BatchNormalization, Flatten
from keras.models import Model
import os
import numpy as np
import tensorflow as tf
import pickle
from sklearn.model_selection import train_test_split
import argparse

# Load the data
path_to_reality = os.path.join('/home/icostley/reu/Reality_mining/Creating_Data', 'reality.csv')
df_init = pd.read_csv(path_to_reality)
df = df_init.drop('End', axis=1)
nodes = max(max(df['Source'].unique()), max(df['Target'].unique()))

# Defining initial variables
# They can be changed with the arg parser
matrix_width = 4
vector_length = int((nodes ** 2 - nodes) / 2)
validation_size = 10000
time_step = 1000
edge_number = 100
layers = None
model_type = 'basic_auto'
epochs = 50
batch_size = 128
optimizer = 'adadelta'
# For loss, Y is specified first
loss_function = ('mse', 'mse')
data_type = 'time'
return_model = False
sk_split = True
loss_weights = (0.5,0.5)

########## Necessary metrics

def total_acc(y_pred, y_true):
    '''
    :param y_pred:
    :param y_true:
    :return: Total accuracy of the network, this includes zeros
    	total_acc just returns the precentage of numbers in y_pred that match y_true
    '''

    #changes tensors to int and rounds predicted y vector
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')
    y_true_int = tf.cast(y_true, dtype='int32')

    #calculate total correct values and total number of values overall
    bool_mat = tf.equal(y_pred_int, y_true_int)
    total_correct = tf.cast(tf.reduce_sum(tf.cast(bool_mat, dtype='float32')), dtype='int32')

    # It does not matter which y vector we take the size of, they both should be equal
    total = tf.cast(tf.size(y_pred_int), dtype='int32')

    return (tf.divide(total_correct, total))

def non_zero(y_pred, y_true):
    '''
    non_zero
    :param y_pred:
    :param y_true:
    :return: Accuracy of the model on non-zero entries for both y_true and y_pred
        (all indices where either y_pred or y_true has a value that is nonzero)
        This was made when we realized y_true_non_zero is not checking all non_zero indices
    '''

    # There should not be a reason to round the true
    # But we do for the sake of symmetry
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')
    y_true_int = tf.cast(tf.round(y_true), dtype='int32')

    #logic_1 is all the nonzeros of y_pred, logic_2 is all the nonzeros of y_true
    logic_1 = tf.not_equal(y_pred_int, 0)
    logic_2 = tf.not_equal(y_true_int, 0)
    #zero_index: or of logic_1 and logic_2
    zero_index = tf.logical_or(logic_1, logic_2)


    y_pred_sub = tf.boolean_mask(y_pred_int, zero_index)
    y_true_sub = tf.boolean_mask(y_true_int, zero_index)

    #calculate total_correct and total overall
    bool_mat = tf.equal(y_pred_sub, y_true_sub)
    total_correct = tf.cast(tf.reduce_sum(tf.cast(bool_mat, dtype='int32')), dtype='int32')

    # It does not matter which sub vector is chosen
    total = tf.cast(tf.size(y_pred_sub), dtype='int32')

    return (tf.divide(total_correct, total))

def closeness_non_zero(y_pred, y_true):
    '''
    closeness_non_zero
    :param y_pred:
    :param y_true:
    :return: calculates how off the non_zero values are by only subtracting a percentage for
        every unit it is off

    '''
    #Percentage to take off for each integer wrong
    per_off = 0.25

    # There should not be a reason to round the true
    # But we do for the sake of symmetry
    y_true_int = tf.cast(tf.round(y_true), dtype='int32')
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')

    #logic_1 is the nonzero mask for y_pred, and logic_2 is the same for y_true
    logic_1 = tf.not_equal(y_pred_int, 0)
    logic_2 = tf.not_equal(y_true_int, 0)

    zero_index = tf.logical_or(logic_1, logic_2)

    y_pred_sub = tf.boolean_mask(y_pred_int, zero_index)
    y_true_sub = tf.boolean_mask(y_true_int, zero_index)

    # Applies a relu on the subtracted tensor
    reduced_tens = tf.abs( tf.subtract(y_pred_sub, y_true_sub))

    #counts the number of zeros
    correct = tf.reduce_sum(tf.cast(tf.equal(reduced_tens, 0), dtype='int32'))

    # Closeness
    close_vec = tf.boolean_mask(reduced_tens, tf.not_equal(reduced_tens, 0))
    ones = tf.ones([tf.size(close_vec)])

    mult = tf.scalar_mul(per_off, tf.cast(close_vec, dtype='float32'))
    sub = tf.subtract(ones, mult)
    maxim = tf.maximum(tf.zeros(tf.shape(sub)), sub)
    close = tf.reduce_sum(maxim)

    #Find score
    score = tf.add(close, tf.cast(correct, dtype='float32'))

    #Return percentage of score / total
    total = tf.cast(tf.size(y_true_sub), dtype='float32')

    return tf.divide(score, total)


metric_list = [total_acc, non_zero, closeness_non_zero]

#############


# Load in the data
dir_path = '/home/icostley/reu/Reality_mining/Creating_Data'
path_to_X = os.path.join(dir_path, 'X_seq_train_mat_{}_{}_{}.npy'.format(matrix_width, data_type, time_step))
path_to_y = os.path.join(dir_path, 'y_seq_train_mat_{}_{}_{}.npy'.format(matrix_width, data_type, time_step))

X_train = np.load(path_to_X)
y_train = np.load(path_to_y)

X_train, X_validate, y_train, y_validate = train_test_split(X_train, y_train, test_size=0.10, random_state=55)

X_train_hat = np.reshape(X_train, (len(X_train), matrix_width * vector_length))
X_validate_hat = np.reshape(X_validate, (len(X_validate), matrix_width * vector_length))

# Argparser for the loss weights
parser = argparse.ArgumentParser()
parser.add_argument("--loss_weights", help="Loss weights for the model. The Y part of the model is specified first")
args = parser.parse_args()

if args.loss_weights:
    loss_weights = eval(args.loss_weights)


# Define the model
layer_1, layer_2, layer_3 = (2280,1140,2280)
input_vector = Input(shape=(matrix_width, vector_length))
flatten = Flatten()(input_vector)
encoded_1 = Dense(layer_1, activation='relu', name='encoded_1')(flatten)
norm_1 = BatchNormalization(name='norm_1')(encoded_1)
encoded_2 = Dense(layer_2, activation='relu', name='encoded_2')(norm_1)
norm_2 = BatchNormalization(name='norm_2')(encoded_2)

# Autoencdoer part
decodA_1 = Dense(layer_3, activation='relu', name='decoded_Auto_1')(norm_2)
normA_1 = BatchNormalization(name='normA_1')(decodA_1)
decodA_2 = Dense(vector_length * matrix_width, name='decoded_Auto_2')(normA_1)

# Y pred part
decodY_1 = Dense(layer_3, activation='relu', name='decoded_Y_1')(norm_2)
normY_1 = BatchNormalization(name='normY_1')(decodY_1)
decodY_2 = Dense(vector_length, name='decoded_Y_2')(normY_1)

model = Model(input_vector, [decodA_2, decodY_2])



# Fit the model

training_data = (X_train, {'decoded_Y_2': y_train, 'decoded_Auto_2': X_train_hat})
validation_dat = (X_validate, {'decoded_Y_2': y_validate, 'decoded_Auto_2': X_validate_hat})

model.compile(optimizer=optimizer, metrics=metric_list, loss={'decoded_Y_2': loss_function[0], 'decoded_Auto_2': loss_function[1]},
              loss_weights={'decoded_Y_2': loss_weights[0], 'decoded_Auto_2': loss_weights[1]})
history = model.fit(x=training_data[0], y=training_data[1], batch_size=batch_size, epochs=epochs,
                    validation_data=validation_dat)

with open('history_{}_{}_{}_{}_{}_valS_{}_loss_{}_Seq'.format('multi_output', optimizer, loss_function, (layer_1, layer_2, layer_3),
                                                  data_type, sk_split, loss_weights),
          'wb') as pifile:
    pickle.dump(history.history, pifile, protocol=pickle.HIGHEST_PROTOCOL)