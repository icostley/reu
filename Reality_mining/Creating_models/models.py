from keras.layers import Dense, Input, LSTM, BatchNormalization, Flatten
from keras.models import Model
'''
models.py

Marisa O'Gara, Ian Costley

July 6, 2018
A class for our various autoencoder models.
'''

class models:
    def __init__(self, layers, vector_length, matrix_width):
        self.layers = layers
        self.vector_length = vector_length
        self.matrix_width = matrix_width
    '''
    basic_auto
    A simple autoencoder with no recurrent layers and 3 layers overall
    '''
    def basic_auto(self):
        if self.layers == None:
            self.layers = (2280, 1140, 2280)

        if len(self.layers) != 3:
            raise ValueError('"Layers must be tuple of length 3 for "basic_auto"')

        layer_1, layer_2, layer_3 = self.layers
        input_vector = Input(shape=(self.matrix_width, self.vector_length))
        flatten = Flatten()(input_vector)
        encoded = Dense(layer_1, activation='relu', name='encoded_1')(flatten)
        norm = BatchNormalization()(encoded)
        encoded = Dense(layer_2, activation='relu', name='encoded_2')(norm)
        norm = BatchNormalization()(encoded)

        decoded = Dense(layer_3, activation='relu', name='decoded_1')(norm)
        norm = BatchNormalization()(decoded)
        decoded = Dense(self.vector_length, name='decoded_2')(norm)

        basic_autoencoder = Model(input_vector, decoded)

        return basic_autoencoder

    '''
    baseline_lstm
    A simple lstm model with 2 layers
    '''
    def baseline_lstm(self):
        if len(self.layers) != 2:
            raise ValueError('"Layers" must be tuple of length 2 for "baseline_lstm"')

        if self.layers == None:
            self.layers = (self.vector_length // 8, self.vector_length // 16)

        layer_1, layer_2 = self.layers
        input_mat = Input(shape=(self.matrix_width, self.vector_length))
        encoded = LSTM(layer_1, return_sequences=True)(input_mat)
        encoded = LSTM(layer_2, return_sequences=True)(encoded)

        decoded = LSTM(layer_1)(encoded)
        decoded = Dense(self.vector_length)(decoded)

        baseline_long = Model(input_mat, decoded)

        return baseline_long
    '''
    lstm
    Another simple lstm model but layers are a little different

    '''
    def lstm(self):
        if len(self.layers) != 2:
            raise ValueError('"Layers" must be be tuple of length 2 for "lstm"')

        if self.layers == None:
            self.layers = (64,32)

        layer_1, layer_2 = self.layers
        input_mat = Input(shape=(self.matrix_width, self.vector_length))
        lstm_1 = LSTM(layer_1, return_sequences=True)(input_mat)
        lstm_2 = LSTM(layer_2)(lstm_1)

        dense_1 = Dense(self.vector_length)(lstm_2)

        lstm_model = Model(input_mat, dense_1)

        return lstm_model