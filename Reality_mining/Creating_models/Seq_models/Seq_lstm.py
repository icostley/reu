'''
This file should not need to be made
But we are having troubles with memory managemnt
So all functions and models will be contained within this file
'''


import pandas as pd
from keras.layers import Dense, Input, LSTM
from keras.models import Model
import os
import argparse
import numpy as np
import tensorflow as tf
import pickle
from sklearn.model_selection import train_test_split

# Load the data
path_to_reality = os.path.join('/home/icostley/reu/Reality_mining/Creating_Data', 'reality.csv')
df_init = pd.read_csv(path_to_reality)
df = df_init.drop('End', axis=1)
nodes = max(max(df['Source'].unique()), max(df['Target'].unique()))

# Defining initial variables
# They can be changed with the arg parser
matrix_width = 4
vector_length = int((nodes ** 2 - nodes) / 2)
validation_size = 10000
time_step = 1000
edge_number = 100
layers = None
model_type = 'basic_auto'
epochs = 50
batch_size = 128
optimizer = 'adadelta'
loss_function = 'mse'
data_type = 'time'
return_model = False
sk_split = True
layer_1, layer_2 = (2280,1140)

########## Necessary metrics

def total_acc(y_pred, y_true):
    '''
    :param y_pred:
    :param y_true:
    :return: Total accuracy of the network, this includes zeros
    	total_acc just returns the precentage of numbers in y_pred that match y_true
    '''

    #changes tensors to int and rounds predicted y vector
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')
    y_true_int = tf.cast(y_true, dtype='int32')

    #calculate total correct values and total number of values overall
    bool_mat = tf.equal(y_pred_int, y_true_int)
    total_correct = tf.cast(tf.reduce_sum(tf.cast(bool_mat, dtype='float32')), dtype='int32')

    # It does not matter which y vector we take the size of, they both should be equal
    total = tf.cast(tf.size(y_pred_int), dtype='int32')

    return (tf.divide(total_correct, total))

def non_zero(y_pred, y_true):
    '''
    non_zero
    :param y_pred:
    :param y_true:
    :return: Accuracy of the model on non-zero entries for both y_true and y_pred
        (all indices where either y_pred or y_true has a value that is nonzero)
        This was made when we realized y_true_non_zero is not checking all non_zero indices
    '''

    # There should not be a reason to round the true
    # But we do for the sake of symmetry
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')
    y_true_int = tf.cast(tf.round(y_true), dtype='int32')

    #logic_1 is all the nonzeros of y_pred, logic_2 is all the nonzeros of y_true
    logic_1 = tf.not_equal(y_pred_int, 0)
    logic_2 = tf.not_equal(y_true_int, 0)
    #zero_index: or of logic_1 and logic_2
    zero_index = tf.logical_or(logic_1, logic_2)


    y_pred_sub = tf.boolean_mask(y_pred_int, zero_index)
    y_true_sub = tf.boolean_mask(y_true_int, zero_index)

    #calculate total_correct and total overall
    bool_mat = tf.equal(y_pred_sub, y_true_sub)
    total_correct = tf.cast(tf.reduce_sum(tf.cast(bool_mat, dtype='int32')), dtype='int32')

    # It does not matter which sub vector is chosen
    total = tf.cast(tf.size(y_pred_sub), dtype='int32')

    return (tf.divide(total_correct, total))

def closeness_non_zero(y_pred, y_true):
    '''
    closeness_non_zero
    :param y_pred:
    :param y_true:
    :return: calculates how off the non_zero values are by only subtracting a percentage for
        every unit it is off

    '''
    #Percentage to take off for each integer wrong
    per_off = 0.25

    # There should not be a reason to round the true
    # But we do for the sake of symmetry
    y_true_int = tf.cast(tf.round(y_true), dtype='int32')
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')

    #logic_1 is the nonzero mask for y_pred, and logic_2 is the same for y_true
    logic_1 = tf.not_equal(y_pred_int, 0)
    logic_2 = tf.not_equal(y_true_int, 0)

    zero_index = tf.logical_or(logic_1, logic_2)

    y_pred_sub = tf.boolean_mask(y_pred_int, zero_index)
    y_true_sub = tf.boolean_mask(y_true_int, zero_index)

    # Applies a relu on the subtracted tensor
    reduced_tens = tf.abs( tf.subtract(y_pred_sub, y_true_sub))

    #counts the number of zeros
    correct = tf.reduce_sum(tf.cast(tf.equal(reduced_tens, 0), dtype='int32'))

    # Closeness
    close_vec = tf.boolean_mask(reduced_tens, tf.not_equal(reduced_tens, 0))
    ones = tf.ones([tf.size(close_vec)])

    mult = tf.scalar_mul(per_off, tf.cast(close_vec, dtype='float32'))
    sub = tf.subtract(ones, mult)
    maxim = tf.maximum(tf.zeros(tf.shape(sub)), sub)
    close = tf.reduce_sum(maxim)

    #Find score
    score = tf.add(close, tf.cast(correct, dtype='float32'))

    #Return percentage of score / total
    total = tf.cast(tf.size(y_true_sub), dtype='float32')

    return tf.divide(score, total)


metric_list = [total_acc, non_zero, closeness_non_zero]

#############

# Argument parser
parser = argparse.ArgumentParser()
parser.add_argument("--batch_size", type=int, help="Batch Size for training")
parser.add_argument("--layers", help="Layer tuple for network")
args = parser.parse_args()

if args.batch_size:
    batch_size = args.batch_size
if args.layers:
    if len(eval(args.layers)) != 2:
        raise ValueError("LSTM model must be specified two layers")
    layer_1, layer_2 = eval(args.layers)


# Load in the data
dir_path = '/home/icostley/reu/Reality_mining/Creating_Data'
path_to_X = os.path.join(dir_path, 'X_seq_train_mat_{}_{}_{}.npy'.format(matrix_width, data_type, time_step))
path_to_y = os.path.join(dir_path, 'y_seq_train_mat_{}_{}_{}.npy'.format(matrix_width, data_type, time_step))

X_train = np.load(path_to_X)
y_train = np.load(path_to_y)

X_train, X_validate, y_train, y_validate = train_test_split(X_train, y_train, test_size=0.10, random_state=55,
                                                            shuffle=False)


# Define the model

input_mat = Input(shape=(matrix_width, vector_length))
lstm_1 = LSTM(layer_1, return_sequences=True)(input_mat)
lstm_2 = LSTM(layer_2)(lstm_1)

dense_1 = Dense(vector_length)(lstm_2)

model = Model(input_mat, dense_1)



# Fit the model
model.compile(optimizer=optimizer, metrics=metric_list, loss=loss_function)
history = model.fit(x=X_train, y=y_train, batch_size=batch_size, epochs=epochs,
                    validation_data=(X_validate, y_validate), shuffle=False)

with open('history_{}_{}_{}_{}_{}_valS_{}_seq_bs_{}'.format('lstm', optimizer, loss_function, (layer_1, layer_2), data_type, sk_split, batch_size),
          'wb') as pifile:
    pickle.dump(history.history, pifile, protocol=pickle.HIGHEST_PROTOCOL)
