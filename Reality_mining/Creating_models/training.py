#TODO: Implement argparser for edge or time_step

import pandas as pd
from keras.layers import Dense, Input, BatchNormalization, Flatten
from keras.models import Model
import os
import numpy as np
import tensorflow as tf
import pickle
from copy import deepcopy
import argparse
from Reality_functions import *
from models import models
import os
from metrics import *
from sklearn.model_selection import train_test_split

# Load the data
path_to_reality = os.path.join('/home/icostley/reu/Reality_mining/Creating_Data', 'reality.csv')
df_init = pd.read_csv(path_to_reality)
df = df_init.drop('End', axis=1)
nodes = max(max(df['Source'].unique()), max(df['Target'].unique()))

# Defining initial variables
# They can be changed with the arg parser
matrix_width = 5
vector_length = int((nodes ** 2 - nodes) / 2)
validation_size = 10000
time_step = 1000
edge_number = 100
metric_list = [total_acc, non_zero, closeness_non_zero]
layers = None
model_type = 'basic_auto'
epochs = 50
batch_size = 128
optimizer = 'adadelta'
loss_function = 'mse'
data_type = 'time'
return_model = False
sk_split = False


if __name__ == '__main__':
    # Define the argpaser
    parser = argparse.ArgumentParser()
    parser.add_argument("--matrix_width", type=int, help='Pack size for validation')
    parser.add_argument("--time_step", type=int, help='Time interval for snapshot')
    parser.add_argument("--validation_size",type=int,  help='Validation set size')
    parser.add_argument("--model_type", help='What type of model to train')
    parser.add_argument("--optimizer", help='What optimizer to use for back prop')
    parser.add_argument("--metric_list", help='What metrics to use for the function')
    parser.add_argument("--loss_function", help='Loss function for training the model')
    parser.add_argument("--batch_size", type=int, help='Batch size for training the network')
    parser.add_argument("--epochs", type=int, help='Number of epochs to train the model')
    parser.add_argument("--layers", help='Layer tuple for the network')
    parser.add_argument("--datatype", help='Specify the type of data. Is it by time or edge?')
    parser.add_argument("--edge_number", help="Specify the edge number")
    parser.add_argument("--return_model", type=bool,  help="If the program should return a model or not")
    parser.add_argument("--sk_split", type=bool, help="If validation data should be taken from the training data.")
    args = parser.parse_args()

    ################ Bunch of if statements
    #Notice that the order matters here
    if args.matrix_width:
        matrix_width = args.matrix_width
    if args.time_step:
        time_step = args.time_step
    if args.validation_size:
        validation_size = args.validation_size
    if args.model_type:
        model_type = str(args.model_type)
    if args.sk_split:
        sk_split = args.sk_split
    if args.optimizer:
        optimizer = str(args.optimizer)
    if args.metric_list:
        metric_list = args.metric_list
    if args.loss_function:
        loss_function = args.loss_function
    if args.datatype:
        data_type = args.datatype
    if args.batch_size:
        batch_size = args.batch_size
    if args.epochs:
        epochs = args.epochs
    if args.layers:
        layers = eval(args.layers)
    if args.return_model:
        return_model = args.return_model

    if data_type == 'time':
        dir_path = '/home/icostley/reu/Reality_mining/Creating_Data'
        path_to_X = os.path.join(dir_path, 'X_train_mat_{}_{}_{}.npy'.format(matrix_width, data_type, time_step))
        path_to_y = os.path.join(dir_path, 'y_train_mat_{}_{}_{}.npy'.format(matrix_width, data_type, time_step))
    else:
        dir_path = '/home/icostley/reu/Reality_mining/Creating_Data'
        path_to_X = os.path.join(dir_path, 'X_train_mat_{}_{}_{}.npy'.format(matrix_width, data_type, edge_number))
        path_to_y = os.path.join(dir_path, 'y_train_mat_{}_{}_{}.npy'.format(matrix_width, data_type, edge_number))

    ################ End of the if statements

    # Loading in the training data
    X_train = np.load(path_to_X)
    y_train = np.load(path_to_y)

    # Check for validation data
    X_val_check = 'X_validate_mat_{}_time_{}.npy'.format(matrix_width, time_step)
    y_val_check = 'y_validate_mat_{}_time_{}.npy'.format(matrix_width, time_step)
    data_dir = os.listdir('/home/icostley/reu/Reality_mining/Creating_Data')

    if sk_split:
        X_train, X_validate, y_train, y_validate = train_test_split(X_train, y_train, test_size=0.10, random_state=55)

    elif X_val_check in data_dir and y_val_check in data_dir:
        X_validate = np.load(X_val_check)
        y_validate = np.load(y_val_check)

    else:
        # Create the validation data
        # Create validation data based on time
        if data_type == 'time':
            validation_gen = training_data_generator(df, time_step=time_step, matrix_width=matrix_width)
            X_validate = []
            y_validate = []
            for j,i in enumerate(validation_gen):
                if j == (validation_size - 1):
                    break
                X_validate.append(i[0])
                y_validate.append(i[1])

            X_validate = np.asarray(X_validate)
            y_validate = np.asarray(y_validate)
        # Create validation data based on edge number
        else:
            training = training_data_edge(df, edge_number, matrix_width, validation_size)
            X_validate = training[0]
            y_validate = training[1]

    # Define the model
    model = eval('models({}, {}, {} ).{}()'.format(layers, vector_length, matrix_width, model_type))

    model.compile(optimizer=optimizer, metrics=metric_list, loss=loss_function)
    history = model.fit(x=X_train, y=y_train, batch_size=batch_size, epochs=epochs, validation_data=(X_validate, y_validate))

    # Saving all objects
    if return_model:
        model.save('{}_{}_{}_{}_{}_valS_{}.h5'.format(model_type, optimizer, loss_function, layers, data_type, sk_split))

    # Saving the history objects
    with open('history_{}_{}_{}_{}_{}_valS_{}'.format(model_type, optimizer, loss_function, layers, data_type, sk_split), 'wb') as pifile:
        pickle.dump(history.history, pifile, protocol=pickle.HIGHEST_PROTOCOL)
