import tensorflow as tf
'''
metrics.py

Various metric functions that are used to train autoencoders for our reality data. 

Marisa O'Gara, Ian Costley

July 6, 2018
'''

def total_acc(y_pred, y_true):
    '''
    :param y_pred:
    :param y_true:
    :return: Total accuracy of the network, this includes zeros
    	total_acc just returns the precentage of numbers in y_pred that match y_true
    '''

    #changes tensors to int and rounds predicted y vector
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')
    y_true_int = tf.cast(y_true, dtype='int32')

    #calculate total correct values and total number of values overall
    bool_mat = tf.equal(y_pred_int, y_true_int)
    total_correct = tf.cast(tf.reduce_sum(tf.cast(bool_mat, dtype='float32')), dtype='int32')

    # It does not matter which y vector we take the size of, they both should be equal
    total = tf.cast(tf.size(y_pred_int), dtype='int32')

    return (tf.divide(total_correct, total))


def y_true_non_zero(y_pred, y_true):
    '''
    y_true_non_zero
    :param y_pred:
    :param y_true:
    :return: Accuracy of the model on non-zero entries for the y_true vector

    '''

    # Cast to integers
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')
    y_true_int = tf.cast(y_true, dtype='int32')

    #Find the indices where tenor is not zero in y_true vector
    mat_index = tf.not_equal(y_true_int, 0)
    y_pred_sub = tf.boolean_mask(y_pred_int, mat_index)
    y_true_sub = tf.boolean_mask(y_true_int, mat_index)

    #calculate total correct and total for nonzero entries on y_true
    bool_mat = tf.equal(y_pred_sub, y_true_sub)
    total_correct = tf.cast(tf.reduce_sum(tf.cast(bool_mat, dtype='float32')), dtype='int32')
    total = tf.cast(tf.size(y_pred_sub), 'int32')


    return (tf.divide(total_correct, total))


def non_zero(y_pred, y_true):
    '''
    non_zero
    :param y_pred:
    :param y_true:
    :return: Accuracy of the model on non-zero entries for both y_true and y_pred
        (all indices where either y_pred or y_true has a value that is nonzero)
        This was made when we realized y_true_non_zero is not checking all non_zero indices
    '''

    # There should not be a reason to round the true
    # But we do for the sake of symmetry
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')
    y_true_int = tf.cast(tf.round(y_true), dtype='int32')

    #logic_1 is all the nonzeros of y_pred, logic_2 is all the nonzeros of y_true
    logic_1 = tf.not_equal(y_pred_int, 0)
    logic_2 = tf.not_equal(y_true_int, 0)
    #zero_index: or of logic_1 and logic_2
    zero_index = tf.logical_or(logic_1, logic_2)


    y_pred_sub = tf.boolean_mask(y_pred_int, zero_index)
    y_true_sub = tf.boolean_mask(y_true_int, zero_index)

    #calculate total_correct and total overall
    bool_mat = tf.equal(y_pred_sub, y_true_sub)
    total_correct = tf.cast(tf.reduce_sum(tf.cast(bool_mat, dtype='int32')), dtype='int32')

    # It does not matter which sub vector is chosen
    total = tf.cast(tf.size(y_pred_sub), dtype='int32')

    return (tf.divide(total_correct, total))


def closeness_non_zero(y_pred, y_true):
    '''
    closeness_non_zero
    :param y_pred:
    :param y_true:
    :return: calculates how off the non_zero values are by only subtracting a percentage for
        every unit it is off

    '''
    #Percentage to take off for each integer wrong
    per_off = 0.25

    # There should not be a reason to round the true
    # But we do for the sake of symmetry
    y_true_int = tf.cast(tf.round(y_true), dtype='int32')
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')

    #logic_1 is the nonzero mask for y_pred, and logic_2 is the same for y_true
    logic_1 = tf.not_equal(y_pred_int, 0)
    logic_2 = tf.not_equal(y_true_int, 0)

    zero_index = tf.logical_or(logic_1, logic_2)

    y_pred_sub = tf.boolean_mask(y_pred_int, zero_index)
    y_true_sub = tf.boolean_mask(y_true_int, zero_index)

    # Applies a relu on the subtracted tensor
    reduced_tens = tf.abs( tf.subtract(y_pred_sub, y_true_sub))

    #counts the number of zeros
    correct = tf.reduce_sum(tf.cast(tf.equal(reduced_tens, 0), dtype='int32'))

    # Closeness
    close_vec = tf.boolean_mask(reduced_tens, tf.not_equal(reduced_tens, 0))
    ones = tf.ones([tf.size(close_vec)])

    mult = tf.scalar_mul(per_off, tf.cast(close_vec, dtype='float32'))
    sub = tf.subtract(ones, mult)
    maxim = tf.maximum(tf.zeros(tf.shape(sub)), sub)
    close = tf.reduce_sum(maxim)

    #Find score
    score = tf.add(close, tf.cast(correct, dtype='float32'))

    #Return percentage of score / total
    total = tf.cast(tf.size(y_true_sub), dtype='float32')

    return tf.divide(score, total)
