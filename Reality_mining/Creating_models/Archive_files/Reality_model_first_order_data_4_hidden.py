import pandas as pd
from keras.layers import Dense, Input, BatchNormalization, Flatten
from keras.models import Model
import os
import numpy as np
import tensorflow as tf
import pickle
from copy import deepcopy


# Loading in the data
path_to_reality = os.path.join('/home/icostley/Reality/Creating_Data', 'reality.csv')
df_init = pd.read_csv(path_to_reality)
df = df_init.drop('End', axis=1)
nodes = max(max(df['Source'].unique()), max(df['Target'].unique()))

### Defining variables
matrix_width = 5
vector_length = int((nodes ** 2 - nodes) / 2)
validation_size = 5000
layer_1 = 2280
layer_2 = 1140
layer_3 = 570
layer_4 = 1140
layer_5 = 2280

path_to_data_X = os.path.join('/home/icostley/Reality/Creating_Data', 'X_train_mat_5_time_1000.npy')
path_to_data_y = os.path.join('/home/icostley/Reality/Creating_Data', 'y_train_mat_5_time_1000.npy')

X_train = np.load(path_to_data_X)
y_train = np.load(path_to_data_y)

# Creating some validation data
# Necessary Functions

def create_vector(timerange, dataframe, nodes):
    '''
    create_vector

    requires: tuple timerange, data datframe, int nodes
    modifies: nothing
    effects: Creates a vector of size nodes squared - nodes out of dataframe in timerange
      Essentially taking data and making adjacency matrix in one dimension vector
    '''

    global x

    y = deepcopy(dataframe)

    data_create_vector = y.loc[(y['Start'] >= timerange[0]) & (y['Start'] <= timerange[1])]

    x = np.zeros((int((nodes ** 2 - nodes) / 2),))

    data_create_vector.apply(update_matrix, axis=1)

    return x


def update_matrix(source_target_pair):
    set_ = (source_target_pair[0], source_target_pair[1])
    reset = (min(set_) - 1, max(set_) - 1)
    index = nodes * (reset[0]) - reset[0] * (1 + reset[0]) / 2 + (reset[1] - reset[0] - 1)

    x[int(index)] += 1


def training_data_generator(dataframe_train, time_step, matrix_width):
    '''

    :param dataframe_train: Data frame that has a "Start", "Source", and "Target" column
    :param time_step: What time step should each vector represent
    :param matrix_width: How many vector per x sample
    :return: tuple of x and y
    '''

    max_time = max(dataframe_train['Start'])
    # df_new is adjusted for time bounds
    df_new = dataframe_train.loc[dataframe_train['Start'] <= (max_time - time_step * (matrix_width + 1))]

    nodes = max(max(dataframe_train['Source'].unique()), max(dataframe_train['Target'].unique()))

    while (1):
        i = np.rint(np.random.uniform(low=0, high=(len(df_new) - 1)))

        time = df_new['Start'].iloc[int(i)]


        x_batch = []
        y_batch = None
        for j in range(matrix_width + 1):
            if j == (matrix_width):
                y_batch = create_vector((time + j * time_step, time + (j + 1) * time_step), dataframe_train, nodes)

            else:
                x_batch.append(
                    create_vector((time + j * time_step, time + (j + 1) * time_step), dataframe_train, nodes))


        yield (np.asarray(x_batch), np.asarray(y_batch))

validation_gen = training_data_generator(df, time_step=1000, matrix_width=matrix_width)
X_validate = []
y_validate = []
for j,i in enumerate(validation_gen):
    if j == (validation_size - 1):
        break
    X_validate.append(i[0])
    y_validate.append(i[1])

X_validate = np.asarray(X_validate)
y_validate = np.asarray(y_validate)

# Creating the network

input_vector = Input(shape=(matrix_width, vector_length))
flatten = Flatten()(input_vector)
encoded = Dense(layer_1, activation='relu', name='encoded_1')(flatten)
norm = BatchNormalization()(encoded)
encoded = Dense(layer_2, activation='relu', name='encoded_2')(norm)
norm = BatchNormalization()(encoded)
encoded = Dense(layer_3, activation='relu', name='encoded_3')(norm)
norm  =  BatchNormalization()(encoded)

decoded = Dense(layer_4, activation='relu', name='decoded_1')(norm)
norm = BatchNormalization()(decoded)
decoded = Dense(layer_5, activation='relu', name='decoded_2')(norm)
norm = BatchNormalization()(decoded)
decoded = Dense(vector_length, name='decoded_3')(norm)

model = Model(input_vector, decoded)


# Custom Metric
def total_acc(y_pred, y_true):
    '''

    :param y_pred:
    :param y_true:
    :return: Total accuracy of the network, this includes zeros
    '''
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')
    y_true_int = tf.cast(y_true, dtype='int32')

    bool_mat = tf.equal(y_pred_int, y_true_int)
    total_correct = tf.cast(tf.reduce_sum(tf.cast(bool_mat, dtype='float32')), dtype='int32')
    total = tf.cast(tf.shape(y_pred_int)[0] * tf.shape(y_pred_int)[1], dtype='int32')

    return (tf.divide(total_correct, total))


def y_true_non_zero(y_pred, y_true):
    '''

    :param y_pred:
    :param y_true:
    :return: Accuracy of the model on non-zero entries
    '''

    # Cast to integers
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')
    y_true_int = tf.cast(y_true, dtype='int32')

    # Find the indices where tenor is not zero
    mat_index = tf.not_equal(y_true_int, 0)
    y_pred_sub = tf.boolean_mask(y_pred_int, mat_index)
    y_true_sub = tf.boolean_mask(y_true_int, mat_index)

    bool_mat = tf.equal(y_pred_sub, y_true_sub)
    total_correct = tf.cast(tf.reduce_sum(tf.cast(bool_mat, dtype='float32')), dtype='int32')

    total = tf.cast(tf.size(y_pred_sub), 'int32')

    return (tf.divide(total_correct, total))


# Actually training the model
model.compile(optimizer='adadelta', loss='mse', metrics=[total_acc, y_true_non_zero])
history = model.fit(X_train, y_train, epochs=50, batch_size=128, validation_data=(X_validate, y_validate))

# Saving all objects
model.save('First_order_model_mse_adadelta_{}_{}_{}.h5'.format(layer_1, layer_2, layer_3))

# Saving the history objects
with open('history_First_order_model_mse_adadelta_{}_{}_{}'.format(layer_1, layer_2, layer_3), 'wb') as pifile:
    pickle.dump(history.history, pifile, protocol=pickle.HIGHEST_PROTOCOL)



