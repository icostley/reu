import pandas as pd
from keras.layers import Dense, Input, BatchNormalization, Flatten
from keras.models import Model
import os
import numpy as np
import tensorflow as tf
import pickle
from copy import deepcopy


# Loading in the data
path_to_reality = os.path.join('/home/icostley/Reality/Creating_Data', 'reality.csv')
df_init = pd.read_csv(path_to_reality)
df = df_init.drop('End', axis=1)
nodes = max(max(df['Source'].unique()), max(df['Target'].unique()))

### Defining variables
matrix_width = 5
# Notice that vector length is different for second order data
vector_length = int((nodes ** 2 - nodes) / 2) + nodes
validation_size = 5000
time_step = 1000
layer_1 = 2280
layer_2 = 1140
layer_3 = 2280

path_to_data_X = os.path.join('/home/icostley/Reality/Creating_Data', 'X_train_mat_5_time_1000_eigen_1.npy')
path_to_data_y = os.path.join('/home/icostley/Reality/Creating_Data', 'y_train_mat_5_time_1000_eigen_1.npy')

X_train = np.load(path_to_data_X)
y_train = np.load(path_to_data_y)
y_train = np.resize(y_train, (80000,X_train.shape[-1]))

# Creating some validation data
# Necessary Functions

def create_vector(timerange, dataframe, nodes):
    '''
    create_vector

    requires: tuple timerange, data datframe, int nodes
    modifies: nothing
    effects: Creates a vector of size nodes squared - nodes out of dataframe in timerange
      Essentially taking data and making adjacency matrix in one dimension vector
    '''

    global x

    y = deepcopy(dataframe)

    data_create_vector = y.loc[(y['Start'] >= timerange[0]) & (y['Start'] <= timerange[1])]

    x = np.zeros((int((nodes ** 2 - nodes) / 2),))

    data_create_vector.apply(update_matrix, axis=1)

    return x

def update_matrix(source_target_pair):
    '''
    This is primarily an accessory function for "create_vector"
    :param source_target_pair: Node Pair
    :return: Updates the output vector based on the node pair
    '''
    set_ = (source_target_pair[0], source_target_pair[1])
    reset = (min(set_) - 1, max(set_) - 1)
    index = nodes * (reset[0]) - reset[0] * (1 + reset[0]) / 2 + (reset[1] - reset[0] - 1)

    x[int(index)] += 1



def get_adj_time_range(timerange, dataframe, nodes):
    '''
    requires: tuple timerange, data dataframe, int nodes
    modifies: nothing
    effects: Creates adjacency matrix from given timerange and dataframe. Sorry about
      misleading name. Originally from infectious
    '''

    df_copy = deepcopy(dataframe)

    # take slice of dataframe based on time
    dataframe_slice = df_copy.loc[(dataframe['Start'] >= timerange[0]) & (df_copy['Start'] <= timerange[1])]

    x = np.zeros((nodes, nodes))

    # fill adj. matrix
    for i, j in zip(dataframe_slice['Source'], dataframe_slice['Target']):
        x[int(i - 1)][int(j - 1)] += 1
        x[int(j - 1)][int(i - 1)] += 1

    return x



def eigen_centrality(adjacency_matrix):
    '''
    Requires: adjacency matrix
    Modifies: nothing
    Effects: Takes in adjaency matrix and returns a vector of the eigenvector centralities
      for each node based on the matrix.
    '''

    eigen_all = np.linalg.eig(adjacency_matrix)

    index = np.argmax(eigen_all[0])
    eigen_vector = np.real(eigen_all[1][index])

    normalized = np.absolute(eigen_vector / np.linalg.norm(eigen_vector))

    return normalized

def training_eigen_1(dataframe_train, time_step, matrix_width, sample_size):
    '''

    :param dataframe_train: Data frame
    :param time_step: How long is the snapshot
    :param matrix_width: Pack size
    :param sample_size: How many samples
    :return: Same as training data, except it adds the eigenvector centrality to the end of it
    '''
    X_train = []
    Y_train = []
    max_time = max(dataframe_train['Start'])

    # df_new is adjusted for time bounds
    df_new = dataframe_train.loc[dataframe_train['Start'] <= (max_time - time_step * (matrix_width + 1))]

    nodes = max(max(dataframe_train['Source'].unique()), max(dataframe_train['Target'].unique()))

    # ??
    index_list = np.rint(np.random.uniform(low=0, high=(len(df_new) - 1), size=sample_size))

    for i in index_list:

        time = df_new['Start'].iloc[int(i)]
        x_batch = []
        y_batch = []

        for j in range(matrix_width + 1):
            # Define the time range
            tr = (time + j * time_step, time + (j + 1) * time_step)
            eigen_vector = eigen_centrality(get_adj_time_range(tr, dataframe=dataframe_train, nodes=nodes))
            first_order_vector = create_vector(timerange=tr, dataframe=dataframe_train, nodes=nodes)

            second_order = np.append(first_order_vector, eigen_vector)

            if j == (matrix_width):
                y_batch.append(second_order)

            else:
                x_batch.append(second_order)

        X_train.append(x_batch)
        Y_train.append(y_batch)

    return np.asarray(X_train), np.asarray(Y_train)

validation_data =  training_eigen_1(dataframe_train=df, time_step=time_step, matrix_width=matrix_width, sample_size=validation_size)
X_validate = validation_data[0]
y_validate = np.resize(validation_data[1], (validation_size, vector_length))


# Creating the network

input_vector = Input(shape=(matrix_width, vector_length))
flatten = Flatten()(input_vector)
encoded = Dense(layer_1, activation='relu', name='encoded_1')(flatten)
norm = BatchNormalization()(encoded)
encoded = Dense(layer_2, activation='relu', name='encoded_2')(norm)
norm = BatchNormalization()(encoded)

decoded = Dense(layer_3, activation='relu', name='decoded_1')(norm)
norm = BatchNormalization()(decoded)
decoded = Dense(vector_length, name='decoded_2')(norm)

model = Model(input_vector, decoded)


# Custom Metric
def total_acc(y_pred, y_true):
    '''

    :param y_pred:
    :param y_true:
    :return: Total accuracy of the network, this includes zeros
    '''
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')
    y_true_int = tf.cast(y_true, dtype='int32')

    bool_mat = tf.equal(y_pred_int, y_true_int)
    total_correct = tf.cast(tf.reduce_sum(tf.cast(bool_mat, dtype='float32')), dtype='int32')
    total = tf.cast(tf.shape(y_pred_int)[0] * tf.shape(y_pred_int)[1], dtype='int32')

    return (tf.divide(total_correct, total))


def y_true_non_zero(y_pred, y_true):
    '''

    :param y_pred:
    :param y_true:
    :return: Accuracy of the model on non-zero entries
    '''

    # Cast to integers
    y_pred_int = tf.cast(tf.round(y_pred), dtype='int32')
    y_true_int = tf.cast(y_true, dtype='int32')

    # Find the indices where tenor is not zero
    mat_index = tf.not_equal(y_true_int, 0)
    y_pred_sub = tf.boolean_mask(y_pred_int, mat_index)
    y_true_sub = tf.boolean_mask(y_true_int, mat_index)

    bool_mat = tf.equal(y_pred_sub, y_true_sub)
    total_correct = tf.cast(tf.reduce_sum(tf.cast(bool_mat, dtype='float32')), dtype='int32')

    total = tf.cast(tf.size(y_pred_sub), 'int32')

    return (tf.divide(total_correct, total))


# Actually training the model
model.compile(optimizer='adadelta', loss='mse', metrics=[total_acc, y_true_non_zero])
history = model.fit(X_train, y_train, epochs=50, batch_size=128, validation_data=(X_validate, y_validate))

# Saving all objects
model.save('Second_order_model_mse_adadelta_{}_{}_{}.h5'.format(layer_1, layer_2, layer_3))

# Saving the history objects
with open('history_Second_order_model_mse_adadelta_{}_{}_{}'.format(layer_1, layer_2, layer_3), 'wb') as pifile:
    pickle.dump(history.history, pifile, protocol=pickle.HIGHEST_PROTOCOL)




