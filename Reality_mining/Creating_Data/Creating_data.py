from Reality_functions import *
import pandas as pd
from copy import deepcopy
import numpy as np

#Loading the data
df_init = pd.read_csv('reality.csv')
df = df_init.drop('End', axis=1)
nodes = max(max(df['Source'].unique()), max(df['Target'].unique()))

matrix_width = 5
edge_number = 100
training_size = 5000
time_step = 1000

training = training_data(dataframe_train=df, matrix_width=matrix_width, sample_size=training_size, time_step=time_step)

np.save('X_validate_mat_5_time_{}'.format(time_step), training[0])
np.save('y_validate_mat_5_time_{}'.format(time_step), training[1])
