import pandas as pd
from copy import deepcopy
import numpy as np

#Loading the data
df_init = pd.read_csv('reality.csv')
df = df_init.drop('End', axis=1)
nodes = max(max(df['Source'].unique()), max(df['Target'].unique()))

matrix_width = 5
time_step = 1000
training_size = 80000

# Necessary functions
def create_vector(timerange, dataframe, nodes):
    '''
    create_vector

    requires: tuple timerange, data datframe, int nodes
    modifies: nothing
    effects: Creates a vector of size nodes squared - nodes out of dataframe in timerange
      Essentially taking data and making adjacency matrix in one dimension vector
    '''

    global x

    y = deepcopy(dataframe)

    data_create_vector = y.loc[(y['Start'] >= timerange[0]) & (y['Start'] <= timerange[1])]

    x = np.zeros((int((nodes ** 2 - nodes) / 2),))

    data_create_vector.apply(update_matrix, axis=1)

    return x

def update_matrix(source_target_pair):
    '''
    This is primarily an accessory function for "create_vector"
    :param source_target_pair: Node Pair
    :return: Updates the output vector based on the node pair
    '''
    set_ = (source_target_pair[0], source_target_pair[1])
    reset = (min(set_) - 1, max(set_) - 1)
    index = nodes * (reset[0]) - reset[0] * (1 + reset[0]) / 2 + (reset[1] - reset[0] - 1)

    x[int(index)] += 1



def get_adj_time_range(timerange, dataframe, nodes):
    '''
    requires: tuple timerange, data dataframe, int nodes
    modifies: nothing
    effects: Creates adjacency matrix from given timerange and dataframe. Sorry about
      misleading name. Originally from infectious
    '''

    df_copy = deepcopy(dataframe)

    # take slice of dataframe based on time
    dataframe_slice = df_copy.loc[(dataframe['Start'] >= timerange[0]) & (df_copy['Start'] <= timerange[1])]

    x = np.zeros((nodes, nodes))

    # fill adj. matrix
    for i, j in zip(dataframe_slice['Source'], dataframe_slice['Target']):
        x[int(i - 1)][int(j - 1)] += 1
        x[int(j - 1)][int(i - 1)] += 1

    return x



def eigen_centrality(adjacency_matrix):
    '''
    Requires: adjacency matrix
    Modifies: nothing
    Effects: Takes in adjaency matrix and returns a vector of the eigenvector centralities
      for each node based on the matrix.
    '''

    eigen_all = np.linalg.eig(adjacency_matrix)

    index = np.argmax(eigen_all[0])
    eigen_vector = np.real(eigen_all[1][index])

    normalized = np.absolute(eigen_vector / np.linalg.norm(eigen_vector))

    return normalized

def training_eigen_1(dataframe_train, time_step, matrix_width, sample_size):
    '''

    :param dataframe_train: Data frame
    :param time_step: How long is the snapshot
    :param matrix_width: Pack size
    :param sample_size: How many samples
    :return: Same as training data, except it adds the eigenvector centrality to the end of it
    '''
    X_train = []
    Y_train = []
    max_time = max(dataframe_train['Start'])

    # df_new is adjusted for time bounds
    df_new = dataframe_train.loc[dataframe_train['Start'] <= (max_time - time_step * (matrix_width + 1))]

    nodes = max(max(dataframe_train['Source'].unique()), max(dataframe_train['Target'].unique()))

    # ??
    index_list = np.rint(np.random.uniform(low=0, high=(len(df_new) - 1), size=sample_size))

    for i in index_list:

        time = df_new['Start'].iloc[int(i)]
        x_batch = []
        y_batch = []

        for j in range(matrix_width + 1):
            # Define the time range
            tr = (time + j * time_step, time + (j + 1) * time_step)
            eigen_vector = eigen_centrality(get_adj_time_range(tr, dataframe=dataframe_train, nodes=nodes))
            first_order_vector = create_vector(timerange=tr, dataframe=dataframe_train, nodes=nodes)

            second_order = np.append(first_order_vector, eigen_vector)

            if j == (matrix_width):
                y_batch.append(second_order)

            else:
                x_batch.append(second_order)

        X_train.append(x_batch)
        Y_train.append(y_batch)

    return np.asarray(X_train), np.asarray(Y_train)


training = training_eigen_1(dataframe_train=df, matrix_width=matrix_width, sample_size=training_size, time_step=time_step)

np.save('X_train_mat_5_time_1000_eigen_1', training[0])
np.save('y_train_mat_5_time_1000_eigen_1', training[1])
