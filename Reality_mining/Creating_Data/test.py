import numpy as np

x = np.load('X_train_mat_5_edge_100.npy')
y = np.load('y_train_mat_5_edge_100.npy')

print('X shape: {}'.format(x.shape))
print('y shape: {}'.format(y.shape))
