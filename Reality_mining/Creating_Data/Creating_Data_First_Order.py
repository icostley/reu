import pandas as pd
from copy import deepcopy
import numpy as np
import sys

sys.stdout.write('Wow \n')
# Loading in the data
df_init = pd.read_csv('reality.csv')
df = df_init.drop('End', axis=1)
nodes = max(max(df['Source'].unique()), max(df['Target'].unique()))

matrix_width = 5
time_step = 1000
training_size = 80000


# Necessary Functions

def create_vector(timerange, dataframe, nodes):
    '''
    create_vector

    requires: tuple timerange, data datframe, int nodes
    modifies: nothing
    effects: Creates a vector of size nodes squared - nodes out of dataframe in timerange
      Essentially taking data and making adjacency matrix in one dimension vector
    '''

    global x

    y = deepcopy(dataframe)

    data_create_vector = y.loc[(y['Start'] >= timerange[0]) & (y['Start'] <= timerange[1])]

    x = np.zeros((int((nodes ** 2 - nodes) / 2),))

    data_create_vector.apply(update_matrix, axis=1)

    return x


def update_matrix(source_target_pair):
    set_ = (source_target_pair[0], source_target_pair[1])
    reset = (min(set_) - 1, max(set_) - 1)
    index = nodes * (reset[0]) - reset[0] * (1 + reset[0]) / 2 + (reset[1] - reset[0] - 1)

    x[int(index)] += 1


def training_data_generator(dataframe_train, time_step, matrix_width):
    '''

    :param dataframe_train: Data frame that has a "Start", "Source", and "Target" column
    :param time_step: What time step should each vector represent
    :param matrix_width: How many vector per x sample
    :return: tuple of x and y
    '''

    max_time = max(dataframe_train['Start'])
    # df_new is adjusted for time bounds
    df_new = dataframe_train.loc[dataframe_train['Start'] <= (max_time - time_step * (matrix_width + 1))]

    nodes = max(max(dataframe_train['Source'].unique()), max(dataframe_train['Target'].unique()))

    while (1):
        i = np.rint(np.random.uniform(low=0, high=(len(df_new) - 1)))

        time = df_new['Start'].iloc[int(i)]


        x_batch = []
        y_batch = None
        for j in range(matrix_width + 1):
            if j == (matrix_width):
                y_batch = create_vector((time + j * time_step, time + (j + 1) * time_step), dataframe_train, nodes)

            else:
                x_batch.append(
                    create_vector((time + j * time_step, time + (j + 1) * time_step), dataframe_train, nodes))


        yield (np.asarray(x_batch), np.asarray(y_batch))


# Creating the data

train_gen = training_data_generator(df, time_step=time_step, matrix_width=matrix_width)

X_train = []
y_train = []
for j, i in enumerate(train_gen):
    if j % 100 == 0:
        print('Percentage Complete: {0:.2f}'.format(j / (training_size - 1)))

    elif j == (training_size - 1):
        print('Done!')
        break

    X_train.append(i[0])
    y_train.append(i[1])



X_train_np = np.asarray(X_train)
y_train_np = np.asarray(y_train)

np.save('X_train_mat_{}_time_{}'.format(matrix_width, time_step), X_train_np)
np.save('y_train_mat_{}_time_{}'.format(matrix_width, time_step), y_train_np)